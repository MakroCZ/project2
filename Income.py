from datetime import date


class Income:

    def __init__(self, item, description, amount, measure_unit, value, warehouse, date):
        self.item = item
        self.description = description
        self.amount = amount
        self.measure_unit = measure_unit
        self.value = value
        self.warehouse = warehouse
        self.date = 0
        self.set_date(date)

    # Předělání struktury datumu a uložení jako datum
    def set_date(self, my_date):
        year, month, day = map("".join, zip(*[iter(my_date)]*2))
        if int(year) > 50:
            year = "19" + year
        else:
            year = "20" + year
        self.date = date(int(year), int(month), 1)
