from Income import Income
from Outcome import Outcome


class DataAnalyzer:

    def __init__(self):
        self.income_list = []
        self.outcome_list = []

        self.load_data()

        self.stats_by_item_id()
        self.stats_by_warehouse()


    def load_data(self):
        self.load_income()
        self.load_outcome()

    # Načtení příjmů včetně "opravy" hodnoty při opravných záznamech
    def load_income(self):
        income = open("Data/prijem.txt", "r")
        for line in income:
            item, description, amount, measure_unit, value, warehouse, date = line.split("|")
            amount = float(amount.replace(",", "."))
            value = float(value.replace(",", "."))
            if(amount > 0):
                self.income_list.append(Income(item, description, amount, measure_unit, value, warehouse, date))
            else:
                self.income_list.append(Income(item, description, amount, measure_unit, -value, warehouse, date))
        income.close()

    # Načtení výdajů včetně "opravy" hodnoty při opravných záznamech
    def load_outcome(self):
        outcome = open("Data/vydej.txt", "r")
        for line in outcome:
            item, description, amount, measure_unit, value, commission, warehouse, date, center, product = line.split("|")
            amount = float(amount.replace(",", "."))
            value = float(value.replace(",", "."))
            if (amount > 0):
                self.income_list.append(Income(item, description, amount, measure_unit, value, warehouse, date))
            else:
                self.income_list.append(Income(item, description, amount, measure_unit, -value, warehouse, date))
            self.outcome_list.append(Outcome(item, description, amount, measure_unit, value, commission, warehouse, date, center, product))
        outcome.close()

    # Přeskládání struktury dat pro jednodušší budoucí práci (podle id položky)
    def stats_by_item_id(self):
        data = {}
        for line in self.income_list:
            item_id = line.item
            if (item_id in data):
                data.get(item_id).append(line)
            else:
                data[item_id] = [line]

        for line in self.outcome_list:
            item_id = line.item
            if (item_id in data):
                data.get(item_id).append(line)
            else:
                data[item_id] = [line]

        self.find_most_valuable(data)
        self.find_biggest_amount(data)

    # Nalezení a vypsání prvních 10 položek podle průměrné hodnoty
    def find_most_valuable(self, data):
        average_value_dict = {}
        for key, values in data.items():
            value_sum = 0
            for value in values:
                if (type(value) is Income):
                    value_sum += value.value
                else:
                    value_sum -= value.value

            average = value_sum / len(values)
            average_value_dict[key] = average

        sorted_list = sorted(average_value_dict.items(), key=lambda x: x[1], reverse=True)

        print("10 položek s největší průměrnou hodnotou")
        for i in range(10):
            items = data.get(sorted_list[i][0])
            print("ID: " + items[0].item + "; popis: " + items[0].description + "; hodnota: " + str(sorted_list[i][1]))

        print()

    # Nalezení a vypsání prvních 10 položek podle průměrného množství
    def find_biggest_amount(self, data):
        average_amount_dict = {}
        for key, values in data.items():
            amount_sum = 0
            for value in values:
                if (type(value) is Income):
                    amount_sum += value.amount
                else:
                    amount_sum -= value.amount

            average = amount_sum / len(values)
            average_amount_dict[key] = average

        sorted_list = sorted(average_amount_dict.items(), key=lambda x: x[1], reverse=True)

        print("10 položek s největším průměrným množstvím")
        for i in range(10):
            items = data.get(sorted_list[i][0])
            print("ID: " + items[0].item + "; popis: " + items[0].description + "; množství: " + str(sorted_list[i][1]) + " " + items[0].measure_unit)

        print()

    # Přeskládání struktury dat pro jednodušší budoucí práci (podle id skladu)
    def stats_by_warehouse(self):
        data = {}
        for line in self.income_list:
            item_warehouse = line.warehouse
            if (item_warehouse in data):
                data.get(item_warehouse).append(line)
            else:
                data[item_warehouse] = [line]

        for line in self.outcome_list:
            item_warehouse = line.warehouse
            if (item_warehouse in data):
                data.get(item_warehouse).append(line)
            else:
                data[item_warehouse] = [line]

        self.find_most_valuable_warehouse(data)
        self.find_biggest_amount_warehouse(data)

    # Nalezení a vypsání prvních 10 položek podle průměrné hodnoty
    def find_most_valuable_warehouse(self, data):
        average_value_dict = {}
        for key, values in data.items():
            value_sum = 0
            for value in values:
                if (type(value) is Income):
                    value_sum += value.value
                else:
                    value_sum -= value.value

            average = value_sum / len(values)
            average_value_dict[key] = average

        sorted_list = sorted(average_value_dict.items(), key=lambda x: x[1], reverse=True)

        print("10 skladů s největší průměrnou hodnotou")
        for i in range(10):
            print("Sklad: " + sorted_list[i][0] + "; hodnota: " + str(sorted_list[i][1]))

        print()

    # Nalezení a vypsání prvních 10 položek podle průměrného množství
    def find_biggest_amount_warehouse(self, data):
        average_amount_dict = {}
        for key, values in data.items():
            amount_sum = 0
            for value in values:
                if (type(value) is Income):
                    amount_sum += value.amount
                else:
                    amount_sum -= value.amount

            average = amount_sum / len(values)
            average_amount_dict[key] = average

        sorted_list = sorted(average_amount_dict.items(), key=lambda x: x[1], reverse=True)

        print("10 skladů s největším průměrným množstvím")
        for i in range(10):
            print("Sklad: " + sorted_list[i][0] + "; hodnota: " + str(sorted_list[i][1]))

        print()

app = DataAnalyzer()
